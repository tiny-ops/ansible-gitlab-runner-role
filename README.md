# Ansible role for GitLab Runners

Install and register gitlab runner(s) for RHEL 7/8 and ArchLinux.

Gitlab Runner package weights 400MB+, so it takes time ;)

### How it works

What role does:
- Install gitlab-runner package
- Unregister all gitlab-runners related to current host
- Register gitlab runner(s)

### Variables:

```yaml
gitlab_runner_projects: []

#gitlab_runner_projects:
#  - name: pet project
#    # if runner_name wasn't defined use inventory_hostname
    #runner_name: "test-runner"
#    executor: "docker"
#    docker_image: "node:latest"
#    registration_token: "CHANGE-ME"
#    tags: "linux,node"

gitlab_runner_user: gitlab-runner
gitlab_runner_group: gitlab-runner

gitlab_runner_data_path: /home/gitlab-runner
```

### Example:

```yaml
- hosts: ci
  roles:
  - tinyops.gitlab-runner
```
